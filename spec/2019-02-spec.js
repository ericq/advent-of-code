let { runIntCode } = require('../2019/02/solve');

describe("Check intcodes", function() {
  it("1,9,10,3,2,3,11,0,99,30,40,50 should be 3500,9,10,70,2,3,11,0,99,30,40,50", function() {
    let value = runIntCode([1,9,10,3,2,3,11,0,99,30,40,50]);
    expect(value).toEqual([3500,9,10,70,2,3,11,0,99,30,40,50])
  });

  it("1,0,0,0,99 becomes 2,0,0,0,99", function() {
    let value = runIntCode([1,0,0,0,99]);
    expect(value).toEqual([2,0,0,0,99])
  });

  it("2,3,0,3,99 becomes 2,3,0,6,99", function() {
    let value = runIntCode([2,3,0,3,99]);
    expect(value).toEqual([2,3,0,6,99])
  });

  it("2,4,4,5,99,0 becomes 2,4,4,5,99,9801", function() {
    let value = runIntCode([2,4,4,5,99,0]);
    expect(value).toEqual([2,4,4,5,99,9801])
  });

  it("1,1,1,4,99,5,6,0,99 becomes 30,1,1,4,2,5,6,0,99", function() {
    let value = runIntCode([1,1,1,4,99,5,6,0,99]);
    expect(value).toEqual([30,1,1,4,2,5,6,0,99])
  });
});