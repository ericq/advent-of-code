const fs = require('fs');

const input = fs.readFileSync('./2019/02/input.txt', 'utf8')
  .split(',')
  .map(Number);

function opCode(opcode, firstInput, secondInput ,output) {
  switch(opcode) {
    case 1:
      return firstInput + secondInput;
    case 2:
      return firstInput * secondInput;
  }
}

// Intcode Needs cleanup!!
function runIntCode(intCode) {
  for (let i = 0; i < intCode.length; i+=4) {
    let element = i;
    if (intCode[element] == 99) {
      break;
    }
    intCode[intCode[3 + element]] = opCode(
      intCode[element],
      intCode[(intCode[1 + element])],
      intCode[(intCode[2 + element])]);
  }
  return intCode;
}

input[1] = 12;
input[2] = 2;
console.log(runIntCode(input)[0]);



module.exports = {
  runIntCode
}