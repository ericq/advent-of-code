let { calcModuleTotalFeul } = require("../2019/01/solve");

describe("Calculate module feul usage", function () {
  it("mass of 14, to get 2.", function() {
    const value = calcModuleTotalFeul(12);
    expect(value).toBe(2);
  });

  it("mass of 1969, fuel shoud be 966", function() {
    const value = calcModuleTotalFeul(1969);
    expect(value).toBe(966);
  });

  it("mass of 100756, fuel shoud be 50346", function() {
    const value = calcModuleTotalFeul(100756);
    expect(value).toBe(50346);
  });
});