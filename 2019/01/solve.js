const fs = require('fs');

const input = fs.readFileSync('./2019/01/input.txt', 'utf8')
  .toString()
  .split('\r\n');

function calcModuleFeul(feul) {
  return Math.floor(feul / 3) - 2;
}

function calcModuleTotalFeul(feul) {
  let moduleTotalFeul = 0;

  do {
    feul = calcModuleFeul(feul);
    moduleTotalFeul += ((feul >= 0) ? feul : 0);
  } while (feul > 0)

  return moduleTotalFeul;
}

function getTotalFeul(input) {
  return input.reduce((acc, mass) => {
    return acc + calcModuleTotalFeul(mass);
  }, 0);
}

console.log(getTotalFeul(input));


module.exports = {
  solve: getTotalFeul,
  calcModuleTotalFeul
}